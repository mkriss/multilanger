//
//  AppDelegate.h
//  MultiLanger
//
//  Created by Krišs Miķelsons on 14/03/14.
//  Copyright (c) 2014 Krišs Miķelsons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
