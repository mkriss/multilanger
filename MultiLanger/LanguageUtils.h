//
//  LanguageUtils.h
//  MultiLanger
//
//  Created by Krišs Miķelsons on 14/03/14.
//  Copyright (c) 2014 Krišs Miķelsons. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LanguageUtils : NSObject


@property ( nonatomic, retain) NSString *currentLanguage;

- (NSString *) localizedString:(NSString *)key;


@end
