//
//  ViewController.h
//  MultiLang
//
//  Created by Krišs Miķelsons on 14/03/14.
//  Copyright (c) 2014 Krišs Miķelsons. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageUtils.h"


@interface ViewController : UIViewController{

    LanguageUtils *lang;

}
- (IBAction)countBtn:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UILabel *counter;
@property (strong, nonatomic) IBOutlet UIButton *countBtnOutlet;



- (IBAction)setLatvian:(UIButton *)sender;
- (IBAction)setEnglish:(UIButton *)sender;

- (void) setLanguage:(NSString*) l ;
@end
