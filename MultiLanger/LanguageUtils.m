//
//  LanguageUtils.m
//  MultiLanger
//
//  Created by Krišs Miķelsons on 14/03/14.
//  Copyright (c) 2014 Krišs Miķelsons. All rights reserved.
//

#import "LanguageUtils.h"



@implementation LanguageUtils

@synthesize currentLanguage;


- (NSString *) localizedString:(NSString *)key, ...
{
    if (self.currentLanguage == nil) {
        self.currentLanguage = @"Base";
    }
    va_list ap;
    va_start(ap, key);
    
    NSString* path = [[NSBundle mainBundle] pathForResource:self.currentLanguage ofType:@"lproj"];

    NSBundle* languageBundle = [NSBundle bundleWithPath:path];
    NSString *format = [languageBundle localizedStringForKey:key value:@"" table:nil];
    NSString *result =  [[NSString alloc] initWithFormat:format arguments:ap];
    va_end (ap);
    return result;
}


@end
