//
//  main.m
//  MultiLanger
//
//  Created by Krišs Miķelsons on 14/03/14.
//  Copyright (c) 2014 Krišs Miķelsons. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
